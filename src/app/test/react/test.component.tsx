import React from 'react';

const Test: React.FC<{ currentTime: number, clickCallback: () => {} }> = (props) => {

	return (
		<div>
			<div>这是 Test 组件，测试组件传值</div>
			<div>React: {props.currentTime}</div>
			<a style={{ textDecoration: 'underline', cursor: 'pointer' }}
			   onClick={props.clickCallback}>
				点击此处，回调到 Angular 组件
			</a>
		</div>
	)

}

export default Test;
