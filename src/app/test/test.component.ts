import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnDestroy, SimpleChanges, ViewChild, ViewEncapsulation } from "@angular/core";
import { ReactComponent } from "src/app/react-component";
import Test from "./react/test.component";

@Component({
	encapsulation: ViewEncapsulation.Emulated,
	selector: 'app-test',
	template: `
		<div #react></div>
	`,
})
export class TestComponent implements AfterViewInit, OnChanges, OnDestroy {

	@ViewChild("react", { static: true }) reactEleRef!: ElementRef<HTMLDivElement>;
	@Input() currentTime = 0;

	private reactComponent?: ReactComponent;

	ngAfterViewInit() {
		this.reactComponent = new ReactComponent(this.reactEleRef.nativeElement, Test, {
			currentTime: { value: this.currentTime, state: true },
			clickCallback: { value: () => this.reactClick() }
		});
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.reactComponent?.propsState['currentTime'].set(changes['currentTime'].currentValue);
	}

	ngOnDestroy(): void {
		this.reactComponent?.destroy();
	}

	reactClick(): void {
		console.log('React 组件点击回调');
	}

}
