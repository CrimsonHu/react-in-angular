import React, { ReactNode, useEffect, useRef } from "react";
import styled from "@emotion/styled";

type PropsType = {
	onNumberHeightChange: (e: number) => void,
	children: ReactNode,
}

const maxWidth = 500;

const Container = styled((props: PropsType) => {

	const { onNumberHeightChange, children, ...rest } = props;
	const containerRef = useRef<HTMLDivElement>(null);
	const [vars, setVars] = React.useState<React.CSSProperties>({} as React.CSSProperties);

	const resizeChange = (entry: ResizeObserverEntry) => {
		let containerWidth = entry.target.clientWidth;
		if (containerWidth == 0) {
			return;
		}
		let numberWidth = (containerWidth * 0.8 * 0.8 / 3) * 0.8 / 2;
		setVars({
			'--container-height': entry.target.clientHeight + 'px',
			'--strip-border-radius': numberWidth / 2 + 'px',
			'--number-height': numberWidth + 'px',
			'--number-font-size': (numberWidth > 40 ? 16 : 14) + 'px',
		} as React.CSSProperties);
		onNumberHeightChange(numberWidth);
	}

	useEffect(() => {
		const resizeObserver = new ResizeObserver((entries) => {
			if (!Array.isArray(entries) || !entries.length) {
				return;
			}
			for (let entry of entries) {
				resizeChange(entry);
			}
		});
		containerRef.current && resizeObserver.observe(containerRef.current);
		return (): void => {
			containerRef.current && resizeObserver.unobserve(containerRef.current);
		};
	}, []);

	return (
		<div {...rest} style={vars}>
			<div className='container' ref={containerRef}>{children}</div>
		</div>
	);

})(({}) => ({
	'--antd-dust-red': '#f5222d',
	'--antd-volcano': '#fa541c',
	'--antd-sunset-orange': '#fa8c16',
	'--antd-calendula-gold': '#faad14',
	'--antd-sunrise-yellow': '#fadb14',
	'--antd-lime': '#a0d911',
	'--antd-polar-green': '#52c41a',
	'--antd-cyan': '#13c2c2',
	'--antd-daybreak-blue': '#1890ff',
	'--antd-geek-blue': '#2f54eb',
	'--antd-golden-purple': '#722ed1',
	'--antd-magenta': '#eb2f96',
	'--ion-color-light': '#f4f5f8',

	display: 'flex',
	justifyContent: 'center',
	alignItems: 'center',
	position: 'relative',
	width: '100%',
	height: `100%`,
	background: 'linear-gradient(-45deg, var(--antd-daybreak-blue), var(--antd-cyan))',
	overflow: 'hidden',

	'& > .container': {
		display: 'flex',
		position: 'relative',
		width: '100%',
		maxWidth: `${maxWidth}px`,
		height: '100%',
		overflow: 'hidden',

		'& > .clock': {
			flex: '1',
			display: 'flex',
			flexDirection: 'row',
			gap: '10%',
			padding: '0 10%',
			height: '100%',

			'& > .hr, & > .min, & > .sec': {
				flex: '1',
				display: 'flex',
				flexDirection: 'row',
				justifyContent: 'center',
				alignItems: 'flex-start',
				gap: '20%',
				marginTop: 'calc(var(--container-height) / 2 - var(--number-height) / 2)',

				'& > .strip': {
					flex: '1',
					display: 'flex',
					flexDirection: 'column',
					backgroundColor: 'var(--antd-cyan)',
					borderRadius: 'var(--strip-border-radius)',
					boxShadow: '-16px -16px 32px -8px var(--antd-daybreak-blue), 16px 16px 32px var(--antd-geek-blue)',
					transition: 'transform 500ms ease-in-out',

					'& > .number': {
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
						width: '100%',
						height: 'var(--number-height)',
						color: '#f4f5f8',
						fontSize: 'var(--number-font-size)',
						fontFamily: '"Roboto Mono", monospace',
						borderRadius: '50%',
						transition: 'all 500ms 100ms ease',
						userSelect: 'none',
					},

					'& > .number.pop': {
						color: 'var(--antd-daybreak-blue)',
						fontWeight: 'bold',
						transform: 'scale(1.3)',
						backgroundColor: 'var(--ion-color-light)',
						boxShadow: '-4px -4px 8px -4px var(--antd-daybreak-blue), 4px 4px 8px var(--antd-geek-blue)',
					},
				},
			},
		},
	},
}));

export default Container;
