import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild, ViewEncapsulation } from "@angular/core";
import { ReactComponent } from "src/app/react-component";
import StripClock from "./react/strip-clock.component";

@Component({
	encapsulation: ViewEncapsulation.Emulated,
	selector: 'app-strip-clock',
	template: `
		<div style="width: 100%; height: 100%" #react></div>
	`,
})
export class StripClockComponent implements AfterViewInit, OnDestroy {

	@ViewChild("react", { static: true }) reactEleRef!: ElementRef<HTMLDivElement>;

	private reactComponent?: ReactComponent;

	ngAfterViewInit() {
		this.reactComponent = new ReactComponent(this.reactEleRef.nativeElement, StripClock);
	}

	ngOnDestroy(): void {
		this.reactComponent?.destroy();
	}

}
