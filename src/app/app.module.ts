import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContainerComponent } from "./container/container.component";
import { StripClockComponent } from "./strip-clock/strip-clock.component";
import { TestComponent } from "./test/test.component";

@NgModule({
	declarations: [
		AppComponent,
		ContainerComponent,
		StripClockComponent,
		TestComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
