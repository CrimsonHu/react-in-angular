import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-container',
	templateUrl: './container.component.html',
	styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {

	currentTime = 0;

	ngOnInit(): void {
		setInterval(() => {
			this.currentTime = new Date().getTime();
		}, 200);
	}

}
